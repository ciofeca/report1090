## Description

Fetch ADS-B messages from an RTL-2832 dongle, broadcast on LAN via MQTT, build an HTML page and a PNG map.

Used to update my [ADS-B report blog](https://adsb-traffic.blogspot.com).

![live example](./map.png)

## Pre-requisites

- Ruby (and *ruby-mqtt* gem)
- Gnuplot
- Mosquitto
- world 10M map (fetch it [here](http://www.gnuplotting.org/plotting-the-world-revisited/)).

## Setup

ADS-B receiver box: Beagleboard xM with RTL-2832 and antenna; [Mode S decoder (dump1090)](https://github.com/antirez/dump1090) and a script to broadcast incoming ADS-B messages to LAN via MQTT.

Local server: this *report1090* script:

- "server" mode: collect/store MQTT messages to some *adsb.log* file
- "test": build full HTML page and PNG map
- "publish": build HTML table and PNG map, invoke external script to ship them

## Configuration

Update the *report1090.config* script with:

- your MQTT credentials/topic
- external program to send the email (only used when "publish" mode)
- map window coordinates (latitude/longitude)
- known codes of common flights
- coordinates of main landing strips in nearby airports
