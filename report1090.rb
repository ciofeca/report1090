#!/usr/bin/env ruby

# -----------------------------------------------------------------------------
# setup
#
load "#$0.config"		# configuration constants: ADSB_TEMP_FILE = ...
load 'x1090-airlinecodes.rb'    # FLIGHTID = { "XYZ" => "call name" ... }

require 'mqtt'  if ARGV.first == 'server'
require 'date'

args = %w{ server test publish }
fail "usage: #$0 #{args.join '|'}"  unless ARGV.size == 1 && args.include?(ARGV.first)

# -----------------------------------------------------------------------------
# library
#
def gnuplot inputdata, outputpng, title
	IO.popen('gnuplot', 'w') do |fp|
		fp.puts "set output '#{outputpng}'
			set terminal png large size #{GNUPLOT_SIZE}
			set datafile separator whitespace
			set xrange [ #{GNUPLOT_LONGITUDE_RANGE} ]
			set yrange [ #{GNUPLOT_LATITUDE_RANGE} ]
			set grid"

		if GNUPLOT_AIRWAYS.size > 0
			fp.puts "set label 'airports -' front at graph 0.018,1-0.036 textcolor 'blue'"
			GNUPLOT_AIRWAYS.each_pair do |name, coords|
				c1, c2 = coords.collect { |i| i.rotate.join ',' }
				fp.puts "set object polygon from #{c1} to #{c2} to #{c1} front fillcolor 'blue'"
			end
		end

		fp.puts "plot '#{GNUPLOT_WORLD}' with filledcurves linetype rgb 'green', \
	       			'#{inputdata}' title '#{title}' with points black #{GNUPLOT_STYLE}"
	end
end

# convert position to degrees/min/sec HTML string
#
def degrees n, up, down
	if n < 0
		n = -n
		up = down
	end

	g = n.floor
	r = (n - g) * 60.0
	m = r.floor
	s = ((r - m) * 600.0).floor / 10.0
	"#{g}&ordm;#{m}'#{s}\"#{up}"
end

class String
	def isdigit
		(self =~ /[[:digit:]]/) == 0
	end

	def tag t
		"<#{t}>#{self}</#{t}>"
	end

	def th
		tag 'th'
	end

	def tdc
		"<td align=\"center\">#{self}</td>"
	end

	def td
		tag 'td'
	end

	def i
		tag 'i'
	end

	def b
		tag 'b'
	end

	def li
		tag 'li'
	end

	def tt
		tag 'tt'
	end

	def small
		tag 'small'
	end

	def to_degrees
		lat, lon = split ','
		"#{degrees(lat.to_f,'N','S')},&nbsp;#{degrees(lon.to_f,'E','W')}"
	end
end

# -----------------------------------------------------------------------------
# server loop: fetch mosquitto data, append to local file
#
if ARGV.first == 'server'
	last = ''
	STDERR.puts "#$0 server started"
	while true
		begin
			MQTT::Client.connect(MQTT_CREDENTIALS) do |m|
				m.get(MQTT_TOPIC) do |topic, msg|
					# ignore duplicates, even if different timestamp
					#
					next  if msg[0..-10] == last
					last = msg[0..-10]
					x = msg.chomp.split
					# x:   QTR5GN	40.614686	14.326921	11574	108°	864	2019-09-26.09:48:56

					if "#{x.first}" != ''  &&  x[6] != nil
						# output record: timestamp,name,gps,alt,bearing,speed,seen
						#
						record = [ x[6].sub('.',' '), x.first, "#{x[1]},#{x[2]}", x[3], x[4], x[5] ]
						fp = open(ADSB_INPUT_FILE, 'a')
						fp.puts(record.join "\t")
						fp.close						
						next
					end
				end
			end
		rescue MQTT::ProtocolException
			STDERR.puts "MQTT ProtocolException, trying in 10 seconds..."
                	sleep 10
	        rescue Errno::ECONNREFUSED
			STDERR.puts "MQTT ConnRefused, retrying in 10 seconds..."
                	sleep 10
	        end
	end
end

# -----------------------------------------------------------------------------
# build html table and gnuplot map
#
unless File.exist? ADSB_INPUT_FILE
  STDERR.puts "!--cannot open #{ADSB_INPUT_FILE}"
  exit 0
end

inputfile = ADSB_INPUT_FILE
if ARGV.first == 'publish'
	# move to temporary file and use it as input;
	# temporary files will have to be cleaned manually
	#
	File.rename ADSB_INPUT_FILE, ADSB_TEMP_FILE
	sleep 1	# let the last write sink in
	inputfile = ADSB_TEMP_FILE
end

total, db, dbcoord = 0, {}, []
from, upto, vmin, vmax, amin, amax = nil

# parse input packets; db key is "day and flight"; update min & max values
#
open(inputfile).each_line do |r|
	# input file format (6 tab-separated fields):
	# 2019-09-23 20:53:07	TRA2A	40.469147,14.442627	10972	351	683
	#
	begin
		fields = r.chomp.split("\t")
	rescue
		next
	end
	dbcoord << fields[2].split(',').rotate.join(' ')
	day = fields.first.split.first
	flight, alt, speed = fields[1], fields[3].to_i, fields[5].to_i
	key = "#{day} #{flight}"
	if db[key]
		# skip record only if alt and speed are OK
		next  if db[key][3] != '0' && db[key][5] != '0'
	end

	time = DateTime.parse(fields.first).to_time
	from, upto = [ from, upto, time ].compact.minmax
	vmin, vmax = [ vmin, vmax, speed ].compact.minmax  if speed > 0
	amin, amax = [ amin, amax, alt ].compact.minmax    if alt > 0

	total += 1
	db[key] = fields
end
fail "empty input file #{inputfile}"  if db.size == 0

# build coordinate tempfile for gnuplot
#
fp = open(GNUPLOT_COORD_TMP, 'w')
dbcoord.sort!.uniq!
dbcoord.each { |x|  fp.puts x }
fp.close

# first pass: sort and update the key to full timestamp + flight
#
pass = {}
db.sort.to_h.each_pair do |day_and_flight, fields| 
	full_key = fields[0..1].join("\t")
	pass[full_key] = fields
end

# build html text; add header tags if testing
# 
fp = open(ADSB_HTML_FILE, 'w')

if ARGV.first == 'test'
	title = "ADSB data from #{from.strftime '%F.%T'} to #{upto.strftime '%F.%T'}"
	css = "\nul { text-align:left; list-style-type:square }\ntable { background-color: #f4fff4; padding:0.2em; }\ntd { padding:0.1em; }\n"
	fp.print "<html>#{(title.tag('title')+css.tag('style')).tag 'head'}<body>"
end

tdiff = Time.at(upto-from).to_i
density = ((tdiff / total) / 6) / 10.0

# page summary
#
fp.print '<ul>'
fp.print "statistics spanning #{tdiff/3600} hr #{(tdiff%3600)/60} min, from #{from.strftime '%a %-d %H:%M:%S'} local time".li
fp.print "#{total.to_s.b} flights spotted (one every #{density} minutes)".li
fp.print "speeds ranging from #{vmin} to #{vmax} km/h".li
fp.print "altitudes ranging from #{amin} to #{amax.to_s.b} m".li
fp.print '</ul>'

# flights table header
#
fp.print '<p><div align="center">'
fp.print '<table cellspacing="3" border="1" cellpadding="1" align="center" style="align: center" summary="ADSB data table">'
fp.print '<tr>'
[ 'time', 'flight', 'position', 'altitude (m)', 'bearing', 'speed (km/h)' ].each do |label|
	fp.print label.i.th
end
fp.print "</tr>"

# fill table, starting from most recent flights:
#
pass.sort.reverse.each do |val|
	timestamp, flight, position, altitude, bearing, speed = val.last

	if flight.size >= 4
		id = FLIGHTID[flight[0..2]]
		id = FLIGHTID[flight[0..1]]  if id == nil && flight[3].isdigit
		id = KNOWN_INFO[flight]  if KNOWN_INFO[flight]
	end

	fp.print '<tr>'
	fp.print timestamp.sub(' ', '<br>&nbsp;').tt.td
	
	if id
		id = id.split.map(&:capitalize).join(' ')  if id[0] != '*'
		fp.print "#{flight.b}<br>#{id.i.small}".tdc
	else
		fp.print flight.b.tdc
	end

	speed = '&ndash;'  if speed == '0'
	altitude = '&ndash;'  if altitude == '0'
	fp.print position.to_degrees.tdc
	fp.print altitude.tdc
	fp.print "#{bearing.to_i}&ordm;".i.tdc
	fp.print speed.tdc
	fp.print '</tr>'
end
fp.puts '</table></div>'

# generate plot
#
label = "#{dbcoord.size} ADS-B reports from #{db.size} aircrafts"
gnuplot GNUPLOT_COORD_TMP, GNUPLOT_OUTPUT_PNG, label

# add locally generated image below the table if we're building a local html file
#
if ARGV.first == 'test'
	fp.print '<br><div align="center" style="max-width: 96%"><img src="'
	fp.print GNUPLOT_OUTPUT_PNG.split('/').last
	fp.print '" style="max-width:100%;"></div></body></html>'

	STDERR.puts "gnome-open #{ADSB_HTML_FILE}"
end
fp.close

# invoke external script to send mail and attachment
#
if ARGV.first == 'publish'
	subject = '"Flights report ' + Time.now.strftime('(%F)') + '"'
	exec "#{EMAIL_SENDER} #{EMAIL_ADDRESS} #{subject} #{GNUPLOT_OUTPUT_PNG} < #{ADSB_HTML_FILE}"
end

# ---
